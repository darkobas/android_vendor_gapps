$(call inherit-product, vendor/gapps/common-blobs.mk)

TARGET_GAPPS_ARCH := arm64
IS_PHONE := true

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

PRODUCT_PACKAGES += \
    GoogleTTS \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    SoundPickerPrebuilt \
    talkback

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleFeedback \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PixelSetupWizard \
    PrebuiltGmsCoreQt \
    GmsCore \
    SetupWizardPrebuilt \
    Velvet \
    WellbeingPrebuilt
